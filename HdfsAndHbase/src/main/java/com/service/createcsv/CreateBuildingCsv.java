package com.service.createcsv;

import au.com.anthonybruno.Gen;
import au.com.anthonybruno.generator.defaults.IntGenerator;
import com.github.javafaker.Faker;
import com.util.ICreateCsv;

import java.util.Random;

public class CreateBuildingCsv implements ICreateCsv {
    @Override
    public void createCsv(int noOfFilesToGenerate, String path, int noOfRowsToGenerate) {
        for (int i = 1; i <= noOfFilesToGenerate; i++) {
            Faker faker = Faker.instance();
            Gen.start();
            Gen.start()
                    .addField("building_code", new IntGenerator(101, 110))
                    .addField("total_floors", new IntGenerator(2, 8))
                    .addField("companies_in_the_building", new IntGenerator(3, 15))
                    .addField("cafteria_code", () -> faker.address().countryCode())
                    .generate(noOfRowsToGenerate)
                    .asCsv()
                    .toFile(path + "building" + i + ".csv");
        }
    }

}

package com.service.readwriteprototohdfs;
import com.util.protoobjects.AttendanceOuterClass;
import com.util.protoobjects.BuildingOuterClass;
import com.util.protoobjects.EmployeeOuterClass;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.io.ArrayFile;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;
import com.config.ConfigurationFile;
import java.io.IOException;
import java.util.ArrayList;
import com.config.setVariables;
public class WriteProtoToHdfs {

    public static void writeProtoObjectsToHdfs(ArrayList<EmployeeOuterClass.Employee.Builder> employeeList,
                                               ArrayList<BuildingOuterClass.Building.Builder> buildingList,
                                               ArrayList<AttendanceOuterClass.Attendance.Builder> attendanceList,
                                               String uri, String protoObject) throws Exception {
        ConfigurationFile conf = new ConfigurationFile(setVariables.RESOURCE1,setVariables.RESOURCE2);
        Configuration config = conf.getConfiguration();
        FileSystem fs = FileSystem.get(config);
        SequenceFile.Writer writer = null;
        try {
            Path path = new Path(uri);
            IntWritable key = new IntWritable();
            ImmutableBytesWritable value = new ImmutableBytesWritable();
            int id = 0;

            if (!fs.exists(path)) {
                writer = SequenceFile.createWriter(config, SequenceFile.Writer.file(path),
                        SequenceFile.Writer.keyClass(key.getClass()),
                        ArrayFile.Writer.valueClass(value.getClass()));
                System.out.println("file created");

                if (protoObject.equals("employee")) {
                    for (EmployeeOuterClass.Employee.Builder e : employeeList) {
                        writer.append(new IntWritable((++id)),
                                new  ImmutableBytesWritable(e.build().toByteArray()));
                    }
                } else if(protoObject.equals("building")){
                    for (BuildingOuterClass.Building.Builder b : buildingList) {
                        writer.append(new IntWritable((++id)),
                                new ImmutableBytesWritable(b.build().toByteArray()));
                    }
                }
                else if(protoObject.equals("attendance")){
                    for (AttendanceOuterClass.Attendance.Builder a : attendanceList) {
                        writer.append(new IntWritable((++id)),
                                new ImmutableBytesWritable(a.build().toByteArray()));
                    }
                }
                writer.close();
            } else {
                System.out.println("Sequence file already exit!");
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeStream(writer);
        }
    }
}
